//
//  MainVC.swift
//  ProgettoThun
//
//  Created by Paolo P on 02/02/17.
//  Copyright © 2017 Paolo P. All rights reserved.
//

import UIKit
import WebKit

class MainVC: UIViewController, WKUIDelegate {
    
    var webView: WKWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        view = webView
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(false)
        
        let url = Bundle.main.url(forResource: "index", withExtension: "html", subdirectory: "html")
        let request = URLRequest(url: url!)
        webView.load(request)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print ("error")
    }
    

}
