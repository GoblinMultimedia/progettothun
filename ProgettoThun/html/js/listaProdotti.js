/**
 * Created by paolopiccini on 31/01/17.
 */

var lastClickTime;
var inactivityTimer;
var INACTIVITY_THRESHOLD = 5 * 60 * 1000;

var $feedbackLbl;

var CARICAMENTO_IN_CORSO = ["Caricamento in corso...", "Loading, please wait...", "Laden, bitte warten..."];

$(window).load(function (e) {

    $("img").unveil();

    // inactivity
    lastClickTime = new Date().getTime();
    inactivityTimer = window.setInterval(function () {
        var currentTime = new Date().getTime();
        //console.log(currentTime);
        if (currentTime - lastClickTime > INACTIVITY_THRESHOLD) {
            window.clearTimeout(inactivityTimer);
            window.location = "index.html";
        }
    }, 1000);

    $("body").on("click", function () {
        lastClickTime = new Date().getTime();
    });
    //

    $(document).on("click", ".selectButton", onSelectButtonClicked);

    $("#backBtn").on("click", function () {
        document.location = "selezioneCategorie.html";
    });

    $feedbackLbl = $("#feedback").text(CARICAMENTO_IN_CORSO[$.jStorage.get("language")]);

    alert("http://www.goblindeveloper.it/apps/thun/getListaProdotti.php?selectedLevel=" + getParameters().selectedLevel + "&selectedItem=" + getParameters().selectedItem + "&selectedLanguage=" + getParameters().selectedLanguage);

    $.ajax({
        async: "true",
        type: "POST",
        url: "http://www.goblindeveloper.it/apps/thun/getListaProdotti.php?selectedLevel=" + getParameters().selectedLevel + "&selectedItem=" + getParameters().selectedItem + "&selectedLanguage=" + getParameters().selectedLanguage,
        data: {}
    }).done(function (result) {
        $("#results").html(parseXML(result));
        $feedbackLbl.text("");
    }).error(function (result) {
        alert("Si è verificato un errore");
    });

});

function parseXML(result) {

    console.log(result);

    var htmlString = "";

    $(result).find("data > item").each(function () {

        var descrizione = "";
        descrizione += $(this).find("nomeProdotto").text();
        //descrizione += $(this).find("cat1Livello1").text() + "<br/>";
        //descrizione += $(this).find("cat1Livello2").text() + "<br/>";
        //descrizione += $(this).find("cat1Livello3").text() + "<br/>";
        //descrizione += $(this).find("descrizioneColoreSpecifico").text() + "<br/>";
        //descrizione += $(this).find("materiale").text() + "<br/>";
        //descrizione += $(this).find("cura").text() + "<br/>";
        //descrizione += $(this).find("nomeProdotto").text() + "<br/>";

        var codice = $(this).find("codiceCompleto").text() + "-" + $(this).find("codiceColoreSpecifico").text();
        var filename = codice + "-01.jpg";
        htmlString += "<div class='thumb scrollable'><div class='icon'><img src='http://www.goblindeveloper.it/apps/thun/thumbs/" + filename + "'/></div><div class='description'>" + descrizione + "</div><div class='selectButton' id='selectItemBtn' data-id='" + $(this).find("codiceCompleto").text() + "'><img src='img/select.png'/></div></div>";
        //htmlString += "<div class='thumb scrollable'><div class='icon'><img src='img/back.png'/></div><div class='description'>" + descrizione + "</div></div>";

    });

    return htmlString;
}

function onSelectButtonClicked(e) {
    window.location = "dettaglioProdotto.html?codice=" + $(e.currentTarget).attr("data-id") + "&selectedLanguage=" + getParameters().selectedLanguage;
}


function getParameters() {
    var params = {};
    window.location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (str, key, value) {
        params[key] = value;
    });
    return params;
}
