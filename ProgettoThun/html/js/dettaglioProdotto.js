/**
 * Created by paolopiccini on 09/02/17.
 */

var $feedbackLbl;
var selectedLanguage;
var CARICAMENTO_IN_CORSO = ["Caricamento in corso...", "Loading, please wait...", "Laden, bitte warten..."];

$(window).load(function (e) {

    $feedbackLbl = $("#feedback").text(CARICAMENTO_IN_CORSO[$.jStorage.get("language")]);

    selectedLanguage = getParameters().selectedLanguage;

    $("#backBtn").on("click", function () {
        document.location = "selezioneCategorie.html";
    });

    $.ajax({
        async: "true",
        type: "POST",
        url: "http://www.goblindeveloper.it/apps/thun/getDettaglioProdotto.php?codice=" + getParameters().codice + "&selectedLanguage=" + selectedLanguage,
        data: {}
    }).done(function (result) {
        $("#fullDetails").html(parseXML(result));
        $feedbackLbl.text("");
    }).error(function (result) {
        alert("Si è verificato un errore");
    });

});


function parseXML(result) {

    console.log(result);

    var htmlString = "";
    var filename = "";

    $(result).find("data > item").each(function () {

        htmlString += $(this).find("nomeProdotto").text() + "<br/>";
        htmlString += $(this).find("codiceCompleto").text() + "<br/>";
        htmlString += $(this).find("descrizione").text() + "<br/>";
        htmlString += $(this).find("colore").text() + "<br/>";
        htmlString += $(this).find("misure").text() + "<br/>";
        htmlString += $(this).find("materiale").text() + "<br/>";

        var codiceCompleto = $(this).find("codiceCompleto").text();
        var codiceColoreSpecifico = $(this).find("codiceColoreSpecifico").text();
        filename = codiceCompleto + "-" + codiceColoreSpecifico + "-01.jpg";
        //htmlString += "<div class='thumb scrollable'><div class='icon'><img src='http://www.goblindeveloper.it/apps/thun/thumbs/" + filename + "'/></div><div class='description'>" + descrizione + "</div><div class='selectButton' id='selectItemBtn' data-id='" + codice + "'><img src='img/select.png'/></div></div>";
        //htmlString += "<div class='thumb scrollable'><div class='icon'><img src='img/back.png'/></div><div class='description'>" + descrizione + "</div></div>";

    });

    $("#fullImages").find("img").attr("src", "http://www.goblindeveloper.it/apps/thun/full/" + filename);
    console.log("http://www.goblindeveloper.it/apps/thun/full/" + filename);
    console.log(htmlString);
    return htmlString;
}


function getParameters() {
    var params = {};
    window.location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (str, key, value) {
        params[key] = value;
    });
    return params;
}