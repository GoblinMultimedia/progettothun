/**
 * Created by paolopiccini on 02/02/17.
 */

//var lastClickTime;
//var inactivityTimer;
//var INACTIVITY_THRESHOLD = 1 * 60 * 1000;

$(window).load(function (e) {

    /* inactivity
    lastClickTime = new Date().getTime();
    inactivityTimer = window.setInterval(function () {
        var currentTime = new Date().getTime();
        console.log(currentTime);
        if (currentTime - lastClickTime > INACTIVITY_THRESHOLD) {
            window.clearTimeout(inactivityTimer);
            window.location = "../index.html";
        }
    }, 1000);

    $("body").on("click", function () {
        lastClickTime = new Date().getTime();
    });
    */

    $(".flags").on("click", onLanguageSelected);

});

function onLanguageSelected(e) {
    var selectedLanguage = parseInt($(e.currentTarget).attr("data-id"));
    $.jStorage.set("language", selectedLanguage);
    //window.clearTimeout(inactivityTimer);
    window.location="selezioneCategorie.html";
}
