/**
 * Created by paolopiccini on 02/02/17.
 */

var CARICAMENTO_IN_CORSO = ["Caricamento in corso...", "Loading, please wait...", "Laden, bitte warten..."];

var lastClickTime;
var inactivityTimer;
var INACTIVITY_THRESHOLD = 5 * 60 * 1000;

var $feedbackLbl;
var $containerL1, $containerL2, $containerL3;
var selectedLevel, selectedItem;
var $cercaBtn;

$(window).load(function (e) {

    // inactivity
    lastClickTime = new Date().getTime();
    inactivityTimer = window.setInterval(function () {
        var currentTime = new Date().getTime();
        //console.log(currentTime);
        if (currentTime - lastClickTime > INACTIVITY_THRESHOLD) {
            window.clearTimeout(inactivityTimer);
            window.location = "index.html";
        }
    }, 1000);

    $("body").on("click", function () {
        lastClickTime = new Date().getTime();
    });
    //


    $containerL1 = $("#containerL1");
    $containerL2 = $("#containerL2");
    $containerL3 = $("#containerL3");

    $(document.body).on('click', ".pulsantiL1", onLevel1Selected);
    $(document.body).on('click', ".pulsantiL2", onLevel2Selected);
    $(document.body).on('click', ".pulsantiL3", onLevel3Selected);

    // filter search button

    $cercaBtn = $("#cercaBtn").on("click", onCercaButtonClicked).hide();

    // full catalog

    $("#fullCatalog").on("click", function () {
        window.location = "listaProdotti.html?selectedLevel=0&selectedItem=0&selectedLanguage=" + $.jStorage.get("language");
    });

    // text search

    $("#freeSearchBtn").on("click", onFreeSearchButtonClicked);

    // back button

    $("#backBtn").on("click", function () {
        document.location = "index.html";
    });

    $feedbackLbl = $("#feedback").text(CARICAMENTO_IN_CORSO[$.jStorage.get("language")]);


    $.ajax({
        async: "true",
        type: "POST",
        url: "http://www.goblindeveloper.it/apps/thun/getLevel1.php",
        data: {}
    }).done(function (result) {
        $containerL1.html(parseXML(result, "1"));
        $feedbackLbl.text("");
    }).error(function (result) {
        alert("Si è verificato un errore");
    });

});

function parseXML(result, levelID) {
    var htmlString = "";
    $(result).find("data > item").each(function () {
        if ($(this).text() != "") htmlString += "<div class='pulsantiL pulsantiIdle pulsantiL" + levelID + "'>" + $(this).text() + "</div>"
    });
    return htmlString;
}

function onLevel1Selected(e) {

    $cercaBtn.show();

    $feedbackLbl = $("#feedback").text(CARICAMENTO_IN_CORSO[$.jStorage.get("language")]);
    var selectedL1Item = ($(this).text());

    selectedLevel = 1;
    selectedItem = selectedL1Item;

    $(".pulsantiL1").removeClass("pulsantiSelected").addClass("pulsantiIdle");
    $(this).addClass("pulsantiSelected");
    $containerL2.html("");
    $containerL3.html("");

    $.ajax({
        async: "true",
        type: "POST",
        url: "http://www.goblindeveloper.it/apps/thun/getLevel2.php?selectedL1Item=" + selectedL1Item,
        data: {}
    }).done(function (result) {
        $containerL2.html(parseXML(result, "2"));
        $feedbackLbl.text("");
    }).error(function (result) {
        alert("Si è verificato un errore");
    });

}

function onLevel2Selected(e) {

    $feedbackLbl = $("#feedback").text(CARICAMENTO_IN_CORSO[$.jStorage.get("language")]);
    var selectedL2Item = ($(this).text());

    selectedLevel = 2;
    selectedItem = selectedL2Item;

    $(".pulsantiL2").removeClass("pulsantiSelected").addClass("pulsantiIdle");
    $(this).addClass("pulsantiSelected");
    $containerL3.html("");

    $.ajax({
        async: "true",
        type: "POST",
        url: "http://www.goblindeveloper.it/apps/thun/getLevel3.php?selectedL2Item=" + selectedL2Item,
        data: {}
    }).done(function (result) {
        $containerL3.html(parseXML(result, "3"));
        $feedbackLbl.text("");
    }).error(function (result) {
        alert("Si è verificato un errore");
    });

}

function onLevel3Selected(e) {

    $(".pulsantiL3").removeClass("pulsantiSelected").addClass("pulsantiIdle");
    $(this).addClass("pulsantiSelected");

    var selectedL3Item = ($(this).text());

    selectedLevel = 3;
    selectedItem = selectedL3Item;

}

function onCercaButtonClicked() {

    var lang = ($.jStorage.get("language"));

    if (lang == 0) {
        lang = "IT";
    } else if (lang == 1) {
        lang = "EN";
    } else if (lang == 2) {
        lang = "DE";
    }
    window.location = "listaProdotti.html?selectedLevel=" + selectedLevel + "&selectedItem=" + selectedItem + "&selectedLanguage=" + lang;
}


function onFreeSearchButtonClicked() {
    var searchKey = $("#searchField").text();
    if (searchKey != "") {

        var lang = $.jStorage.get("language").toString();
        if (lang == "0") {
            lang = "IT";
        } else if (lang == "1") {
            lang = "EN";
        } else if (lang == "2") {
            lang = "DE";
        }

        window.location = "listaProdotti.html?selectedLevel=searchKey&selectedItem=" + searchKey + "&selectedLanguage=" + lang;
    }
}